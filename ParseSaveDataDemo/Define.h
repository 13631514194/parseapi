//
//  Define.h
//  ParseSaveDataDemo
//
//  Created by joser on 13-8-19.
//  Copyright (c) 2013年 joser. All rights reserved.
//

#ifndef ParseSaveDataDemo_Define_h
#define ParseSaveDataDemo_Define_h

#define DEFAULT_ALERTVIEW(TEXT)\
UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"提示" message:TEXT delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];\
[alertView show];
#endif
